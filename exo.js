function exo1(){
    let compteur = 10;

    for (compteur ; compteur<=100 ; compteur++) {
        
        console.log(compteur);
        
    }
}

function exo2(){
    let valini = 0;
    let valEnd = 1;
    console.log(valini + " " + valEnd + " ");

    for(let i = 0; i <= 100; i++){
        let valtmp = valini + valEnd;
        valini = valEnd;
        valEnd = valtmp;
        console.log(valEnd + " ");
    }
}

function annex3(stringTest){
    let long = 0;
    while(stringTest[long]){
        long++
    }
    return long;
}

function exo3(){
    let stringTest = "Texte a changer";

    long = annex3(stringTest);

    for(let i = (long-1) ; i >= 0; i--){
        console.log(stringTest[i])
    }
}

function exo4(){
    let star = "*", memory = "";
    for(let i = 0; i <= 9; i++){
        memory += star;
        console.log(memory);
    }
}

function exo5(){
    let star = "*", memory = "";
    for(let i = 0; i <= 10; i++){
        for(let j = 10; j >= i; j--){
            memory += star;
        }
        console.log(memory);
        memory = "";
    }

    
}

function exo6(){
    let nbCopies = prompt("Combien de copies voulues?");
    let prix = 0, tranche10 = 10*0.10, tranche30 = 20*0.09;
    if(nbCopies <= 10){
        prix = nbCopies*0.10;
    } else if(nbCopies > 10 && nbCopies <= 30) {
        prix = tranche10 + (nbCopies - 10)*0.09;
    } else {
        prix = tranche10 + tranche30 + (nbCopies - 30)*0.08;
    }
    console.log(prix.toFixed(2));
}

function exo7(){
    let typeHumain = "femme", age = "36";
    if(typeHumain == "homme" && age >= 18){
        console.log("Tu paye, et cher en plus...");
    } else if(typeHumain == "femme" && age >= 18 && age <= 35){
        console.log("Tu paye Mme "+typeHumain);
    } else {
        console.log("On vous aura un jour...");
    }
}

function exo8(){
    let client = [];
    client[0] = prompt("Veuillez indiquer votre nom? ");
    client[1] = parseInt(prompt("Quel age avez-vous? "));
    client[2] = parseInt(prompt("Depuis combien de temps avez-vous le permis? "));
    client[3] = parseInt(prompt("Combien d'accidents avez-vous eu? "));
    client[4] = 0; // Temps d'adhésion a l'assurance

    // Pondérer le profil
    let noteAge = client[1] >= 25 ? 1 : 0;
    let notePermis = client[2] > 2 ? 1 : 0;
    let noteAccident = client[3] < 1 ? 0 : -1;
    noteAccident = client[3] > 1 ? -4 : noteAccident;
    let noteAssu = client[4] > 0 ? 1 : 0;
    let note = noteAge+notePermis+noteAccident+noteAssu;
    console.log(note);

    switch (note) {
        case 3 || 4:
            alert("Bonjour "+client[0]+", félicitations, grâce à votre conduite et votre fidélité vous obtenez le tarif A.");
            break;
        case 2:
            alert("Bonjour "+client[0]+", vous obtenez le tarif B.");
            break;
        case 1:
            alert("Bonjour "+client[0]+", vous obtenez le tarif C.");
            break;
        case 0:
            alert("Bonjour "+client[0]+", vous obtenez le tarif D.");
            break;
        default:
            alert("Bonjour "+client[0]+", nous somme dans le regret de refuser votre adhésion à notre compagnie d'assurances.")
            break;
    }

}

function exo9(){
    let alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9"," "]
    let saisie = prompt("Ave César, je te rappelle que ton code ne prend pas de ponctuation, je t'écoute Altesse :");
    let cle = parseInt(prompt("Merci de me donner un nombre pour le cryptage :"));
    let longSaisie = annex3(saisie);
    let longAlphabet = annex3(alphabet);
    let code = ''; 
    let indexAlpha = 0;
    
    for(let i = 0; i <= longSaisie - 1; i++){
        //je parcours mon texte => saisie[i]
        while(saisie[i] !== alphabet[indexAlpha]){ // je parcours l'alphabet jusqu'a trouver mon char
        indexAlpha++;
        }
        // si je ne dépasse pas longalphabet je change ma lettre sinon je reprend au début
        if((longAlphabet-1) < (indexAlpha + cle)){
            indexAlpha = (indexAlpha + cle) - longAlphabet;
        } else if((indexAlpha + cle) < 0){
            indexAlpha = (longAlphabet) + ((indexAlpha) + cle);
        } else {
            indexAlpha += cle;
        }
        code += alphabet[indexAlpha];
        indexAlpha = 0;
    }
    
    console.log(code);
    console.log("Clé : "+cle)
}

exo9();
